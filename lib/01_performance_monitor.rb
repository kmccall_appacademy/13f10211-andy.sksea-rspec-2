require 'benchmark'
require 'time'

def measure(t=1, &block)
  total_time = 0.0
  total_time += Benchmark.measure do
    prev = Time.now
    now = block.call
    # Pretty hacky.
    if now.class == Time
      sleep((prev - now).abs) 
      (t-1).times do
        prev, now = now, block.call
        sleep((prev - now).abs) 
      end
    else
      (t-1).times { block.call }
    end
  end.real
  (total_time / t).round(1)
end

# measure 
# - calls the block n times.
# - returns time always.